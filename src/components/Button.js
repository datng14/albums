import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = ({ children, clickEvent }) => {
	const { buttonStyle, textStyle } = styles;
	return (
		<TouchableOpacity onPress={clickEvent} style={buttonStyle}>
			<Text style={textStyle}>{ children }</Text>
		</TouchableOpacity>
	);
};

const styles = {
	textStyle: {
		color: '#007aff',
		alignSelf: 'center',
		fontSize: 16,
		paddingTop: 10,
		paddingBottom: 10,
	},
	buttonStyle: {
		flex: 1,
		backgroundColor: '#fff',
		borderColor: '#007aff',
		alignSelf: 'stretch',
		borderRadius: 5,
		borderWidth: 1,
		marginLeft: 10,
		marginRight: 10,

	}
};

export default Button;
