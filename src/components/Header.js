import React from 'react';
import { View, Text } from 'react-native';

const Header = (props) => {
	const { viewStyles, textStyles } = styles;
	return (
		<View style={viewStyles}>
			<Text style={textStyles}>{props.headerText}</Text>
		</View>
	);
};

const styles = {
	viewStyles: {
		backgroundColor: '#f8f8f8',
		justifyContent: 'center',
		alignItems: 'center',
		paddingTop: 15,
		height: 60,
		shadowColor: '#00',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.2,
		elevation: 2,
		position: 'relative'
	},

	textStyles: {
		fontSize: 40,
		color: 'red',
	}
};

export default Header;
