import React from 'react';
import { Text, Image, View, Linking } from 'react-native';
import Card from './Card';
import CardSection from './CardSection';
import Button from './Button';

// This component only show data, not fetch/get data, so use functional component style
const AlbumDetail = (props) => {
	const { title, artist, thumbnail_image, image, url } = props.album;
	const {
		thumbnailStyle,
		imageStyle,
		headerContentStyle,
		containerThumnailStyle,
		headerTextStyle
	} = styles;

	return (
		<Card>
			<CardSection style={styles.containerStyle}>
				<View style={containerThumnailStyle}>
					<Image
						style={thumbnailStyle}
						source={{ uri: thumbnail_image }}
					/>
				</View>
				<View style={headerContentStyle}>
					<Text style={headerTextStyle}>{title}</Text>
					<Text>{artist}</Text>
				</View>
			</CardSection>

			<CardSection>
				<Image
					style={imageStyle}
					source={{ uri: image }}
				/>
			</CardSection>

			<CardSection>
				{/* clickEvent: can be named whatever you want */}
				<Button clickEvent={() => Linking.openURL(url)}>
					Buy Now
				</Button>
			</CardSection>
		</Card>
	);
};

const styles = {
	thumbnailStyle: {
		width: 50,
		height: 50
	},
	containerStyle: {
		flexDirection: 'row',
	},
	headerContentStyle: {
		justifyContent: 'space-around',
	},
	headerTextStyle: {
		fontSize: 16,
	},
	containerThumnailStyle: {
		alignItems: 'center',
		justifyContent: 'center',
		marginRight: 10,
	},
	imageStyle: {
		height: 300,
		width: null,
		flex: 1,
	}
};

export default AlbumDetail;
